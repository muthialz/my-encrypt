package com.lazuardi.myencrypt;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyRequest {
    public void getJSONObjectRequest(Context mContext, String url, JSONObject jsonValue, final VolleyCallback callback) {
        if (jsonValue == null) {
            jsonValue = new JSONObject();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonValue,
                (JSONObject result) -> callback.onSuccessResponse(result.toString()),
                callback::onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyVolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void goToUrl(Context mContext, String url, final VolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                callback::onSuccessResponse,
                callback::onErrorResponse);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyVolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);
    }
}
