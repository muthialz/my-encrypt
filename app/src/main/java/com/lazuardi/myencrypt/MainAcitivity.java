package com.lazuardi.myencrypt;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class MainAcitivity extends AppCompatActivity {
    private TextView txt_result, txt_result_decrypt;
    private EditText txt_value, txt_value_decrypt;
    private Button btnEncrypt, btnDecrypt;
    private String url_get_value = "http://10.0.2.2/myencrypt/api.php";
    private String action, value;
    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        toolbar.setTitle("My Encrypt Dashboard");

        txt_result = findViewById(R.id.txt_result);
        txt_result_decrypt = findViewById(R.id.txt_result_decrypt);
        txt_value = findViewById(R.id.txt_value);
        txt_value_decrypt = findViewById(R.id.txt_value_decrypt);
        btnEncrypt = findViewById(R.id.btn_send);
        btnDecrypt = findViewById(R.id.btn_send_decrypt);

        btnEncrypt.setOnClickListener(v -> {
            value = txt_value.getText().toString();
            action = "e";
            getValue(true, action, value);
        });

        btnDecrypt.setOnClickListener(v -> {
            value = txt_value_decrypt.getText().toString();
            action = "d";
            getValue(false, action, value);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.signout) {
            logout();
        }
        return super.onOptionsItemSelected(item);
    }

    public void logout() {
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainAcitivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainAcitivity.this);
        }
        builder.setMessage("Do you want to log out?")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {
                    Intent intent = new Intent(MainAcitivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finishAffinity();
                })
                .setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void getValue(Boolean isEncrypt, String action, String value) {
        VolleyRequest request = new VolleyRequest();
        String url = url_get_value;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("value", value);
            jsonObject.put("action", action);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.getJSONObjectRequest(getApplicationContext(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getString("Status").equalsIgnoreCase("success")) {
                        String final_result = jsonResult.getString("Result");
                        if (final_result.equals("false")) {
                            if (isEncrypt) {
                                txt_result.setText("Not in correct format");
                            } else {
                                txt_result_decrypt.setText("Not in correct format");
                            }
                        } else {
                            if (isEncrypt) {
                                txt_result.setText(final_result);
                            } else {
                                txt_result_decrypt.setText(final_result);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Error occurred ", error);
            }
        });

    }
}
